![](https://images.microbadger.com/badges/version/jarylc/murmur.svg) ![](https://images.microbadger.com/badges/image/jarylc/murmur.svg) ![](https://img.shields.io/docker/stars/jarylc/murmur.svg) ![](https://img.shields.io/docker/pulls/jarylc/murmur.svg)

# Environment variables
| Environment | Default value |
|-------------|---------------|
| UID         | 1000          |
| GID         | 1000          |

# Volumes
- /data - murmur configuration and data

# Deploying
## Terminal
```bash
docker run -d \
    --name murmur \
    -e UID=1000 \
    -e GID=1000 \
    -p 64738:64738 \
    -p 64738:64738/udp \
    -v /path/data:/data \
    --restart unless-stopped \
    jarylc/murmur
```
## Docker-compose
```yml
murmur:
    image: jarylc/murmur
    ports:
        - "64738:64738"
        - "64738:64738/udp"
    volumes:
        - /path/data:/data
    environment:
        - UID=1000
        - GID=1000
    restart: unless-stopped
```
