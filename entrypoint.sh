#!/bin/ash

getent group murmur >/dev/null || addgroup -g ${GID} murmur
getent passwd murmur >/dev/null || adduser -h /data -s /bin/sh -G murmur -D -u ${UID} murmur

[[ -f /data/murmur.ini ]] || /bin/cp /etc/murmur.ini /data/murmur.ini

chown -R "${UID}:${GID}" /data

exec su-exec "${UID}:${GID}" "$@"
