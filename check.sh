#!/bin/ash

[[ ! -f EXISTING ]] || touch EXISTING
EXISTING=$(cat EXISTING)
echo "Existing: ${EXISTING}"

apk add murmur
LATEST=$(murmurd -version 2>&1 | tail -n1 | awk -F'-- ' '{print $2}')
echo "Latest: ${LATEST}"

if [[ -n "${LATEST}" && "${LATEST}" != "${EXISTING}" ]]; then
  echo "${LATEST}" > LATEST
  echo "Building..."
fi
