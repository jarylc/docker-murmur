FROM alpine:edge
ENV UID=1000 \
    GID=1000
RUN apk add su-exec murmur && \
    mkdir /data && \
    sed -i 's/var\/lib\/murmur/data/g' /etc/murmur.ini && \
    sed -i 's/var\/log/data/g' /etc/murmur.ini
COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["murmurd", "-fg", "-ini", "/data/murmur.ini"]
